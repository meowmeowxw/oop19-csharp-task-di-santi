using System;
using System.Reflection.Metadata;
using System.Text;
using NUnit.Framework;
using System.Security.Cryptography;

namespace keepassj
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestAesGcm()
        {
            ICryptoCipher cipher = new Aesgcm();
            var plaintext = Encoding.UTF8.GetBytes("This is a plaintext");
            var ad = Encoding.UTF8.GetBytes("This is a the associated data");
            var key = new byte[cipher.KeySize];
            var iv = new byte[cipher.IvSize];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(iv);
            rng.GetBytes(key);
            var ciphertext = cipher.Encrypt(plaintext, key, iv, ad);
            var decrypted = cipher.Decrypt(ciphertext, key, iv, ad);
            Console.WriteLine(plaintext);
            Console.WriteLine(decrypted);
            Assert.AreEqual(plaintext, decrypted);
        }

        [Test]
        public void TestAesGcmAuthentication()
        {
            ICryptoCipher cipher = new Aesgcm();
            var plaintext = Encoding.UTF8.GetBytes("This is a plaintext");
            var ad = Encoding.UTF8.GetBytes("This is a the associated data");
            var key = new byte[cipher.KeySize];
            var iv = new byte[cipher.IvSize];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(iv);
            rng.GetBytes(key);
            var ciphertext = cipher.Encrypt(plaintext, key, iv, ad);
            ad = Encoding.UTF8.GetBytes("This is the corrupted data");
            Assert.Throws<CryptographicException>(() => cipher.Decrypt(ciphertext, key, iv, ad));
        }
    }
}
