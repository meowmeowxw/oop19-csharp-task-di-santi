using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using NUnit.Framework;
using System.Security.Cryptography;

namespace keepassj
{
    public class TestAesCbcHmacSha
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestAesNist()
        {
            var k = Util.HexToBytes("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f");
            var p = Util.HexToBytes("41206369706865722073797374656d206d757374206e6f7420626520726571756972656420746f206265207365637265742c20616e64206974206d7573742062652061626c6520746f2066616c6c20696e746f207468652068616e6473206f662074686520656e656d7920776974686f757420696e636f6e76656e69656e6365");
            var iv = Util.HexToBytes("1af38c2dc2b96ffdd86694092341bc04");
            var a = Util.HexToBytes("546865207365636f6e64207072696e6369706c65206f662041756775737465204b6572636b686f666673");
            var t = Util.HexToBytes("4dd3b4c088a7f45c216839645b2012bf2e6269a8c56a816dbc1b267761955bc5");
            const int indexTag = 144;
            ICryptoCipher aes = CipherFactory.create("AES");
            byte[] c = aes.Encrypt(p, k, iv, a);
            Assert.True(c.StartingIndex(t).First() == indexTag);
            Assert.True(aes.KeySize == 64);
            Assert.AreEqual(p, aes.Decrypt(c, k, iv, a));
        }

        [Test]
        public void TestEncryptionDecryption()
        {
            var p = Encoding.UTF8.GetBytes("THis is a plaintext");
            var k = Util.HexToBytes("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f");
            var iv = Util.HexToBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            var a = Encoding.UTF8.GetBytes("THis is the associated data");
            ICryptoCipher aes = CipherFactory.create("AES");
            byte[] c = aes.Encrypt(p, k, iv, a);
            Assert.AreEqual(p, aes.Decrypt(c, k, iv, a));
        }
    }
}