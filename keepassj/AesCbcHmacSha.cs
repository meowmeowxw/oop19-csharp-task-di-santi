using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace keepassj
{
    public class AesCbcHmacSha:AesAead
    {
        private const int TagSize = 32;
        private const int EncSize = 32;
        private const int MacSize = 32;
        public override int IvSize { get; } = 16;
        public override int KeySize { get; } = 64;
        public override byte[] Encrypt(byte[] plaintext, byte[] key, byte[] iv, byte[] associatedData)
        {
            Init(key, EncSize, MacSize, associatedData);
            using (var aes = Aes.Create())
            {
                 aes.Key = EncKey;
                 aes.IV = iv;
                 aes.Mode = CipherMode.CBC;
                 aes.Padding = PaddingMode.None;
                 var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                 byte[] p = Util.Pad(plaintext, aes.BlockSize / 8);
                 byte[] encrypted = new byte[p.Length];
                 encryptor.TransformBlock(p, 0, p.Length, encrypted, 0);
                 var hmac = new HMACSHA512(MacKey);
                 var x = iv.Concat(encrypted).ToArray();
                 var tag = ComputeHmac(hmac, iv.Concat(encrypted).ToArray(), associatedData, TagSize);
                 return encrypted.Concat(tag).ToArray();
            }
        }

        public override byte[] Decrypt(byte[] ciphertext, byte[] key, byte[] iv, byte[] associatedData)
        {
            Init(key, EncSize, MacSize, associatedData);
            using (var aes = Aes.Create())
            {
                 aes.Key = this.EncKey;
                 aes.IV = iv;
                 aes.Mode = CipherMode.CBC;
                 aes.Padding = PaddingMode.None;
                 var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                 var encrypted = new byte[ciphertext.Length - TagSize];
                 var tag = new byte[TagSize];
                 Array.Copy(ciphertext, 0, encrypted, 0, encrypted.Length);
                 Array.Copy(ciphertext, encrypted.Length, tag, 0, tag.Length);
                 var hmac = new HMACSHA512(this.MacKey);
                 var tagComputed = ComputeHmac(hmac, iv.Concat(encrypted).ToArray(), associatedData, TagSize);
                 if (tagComputed.Except(tag).Count() != 0)
                 {
                     throw new CryptographicException("Bad Tag");
                 }
                 byte[] decrypted = new byte[encrypted.Length];
                 decryptor.TransformBlock(encrypted, 0, encrypted.Length, decrypted, 0);
                 return Util.Unpad(decrypted);
            }
        }
    }
}