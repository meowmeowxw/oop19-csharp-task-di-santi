using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace keepassj
{
    public static class Util
    {
        public static byte[] HexToBytes(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        public static string BytesToHex(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        
        public static byte[] Unpad(byte[] message)
        {
            int padded = message[message.Length - 1];
            for (int i = message.Length - 1; i >= message.Length - padded; i--)
            {
                if (message[i] != Convert.ToByte(padded))
                {
                    throw new CryptographicException("Bad Tag");
                }
            }
            byte[] unpadded = new byte[message.Length - padded];
            Array.Copy(message, 0, unpadded, 0, unpadded.Length);
            return unpadded;
        }
        
        public static byte[] Pad(byte[] message, int blockSize)
        {
            int paddingLength = blockSize - (message.Length % blockSize);
            byte[] paddedMessage = new byte[message.Length + paddingLength];
            Array.Copy(message, 0, paddedMessage, 0, message.Length);
            Array.Fill(paddedMessage, Convert.ToByte(paddingLength), message.Length, paddingLength);
            return paddedMessage;
        }
        
    }
}