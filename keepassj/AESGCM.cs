using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;

namespace keepassj
{
    public class Aesgcm:ICryptoCipher
    {
        private const int TagSize = 16;
        public Aesgcm()
        {
            ;
        }
        public int IvSize { get; } = 12;
        public int KeySize { get; } = 32;

        public byte[] Encrypt(byte[] plaintext, byte[] key, byte[] iv, byte[] associatedData)
        {
            var ciphertext = new byte[plaintext.Length];
            var tag = new byte[TagSize];
            var cipher = new AesGcm(key);
            using (cipher)
            {
                cipher.Encrypt(iv, plaintext, ciphertext, tag, associatedData);
                return tag.Concat(ciphertext).ToArray();
            }
        }

        public byte[] Decrypt(byte[] ciphertext, byte[] key, byte[] iv, byte[] associatedData)
        {
            var tag = new byte[TagSize];
            Array.Copy(ciphertext, 0, tag, 0, TagSize);
            var encrypted = new byte[ciphertext.Length - tag.Length];
            Array.Copy(ciphertext, TagSize, encrypted, 0, encrypted.Length);
            var plaintext = new byte[encrypted.Length];
            var cipher = new AesGcm(key);
            using (cipher)
            {
                cipher.Decrypt(iv, encrypted, tag, plaintext, associatedData);
                return plaintext;
            }
        }
    }
}
