using System;

namespace keepassj
{
    public static class CipherFactory
    {
        public static ICryptoCipher create(String cipher)
        {
            if ("AES".Equals(cipher))
            {
                return new AesCbcHmacSha();
            }
            return new Aesgcm();
        }
    }
}